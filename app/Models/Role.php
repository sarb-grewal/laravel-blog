<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * a role model class that will be used to add roles capabilities to our users
 * this will be used for basic authentication
 */
class Role extends Model
{
    use HasFactory;
}
