<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * here we can check for the user_type of the user
         * if the user is not admin i.e. if user_type is 0, then they will be returned to the login screen
         */
        if (!Auth::user()->user_type) {
            Auth::logout();
            return redirect('login');
        }
        return $next($request);
    }
}
