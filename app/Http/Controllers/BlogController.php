<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\User;
use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'edit', 'store', 'update']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            if ($this->user && $this->user->user_type && in_array(explode('@', Route::currentRouteAction())[1], ['create', 'edit', 'store'])) {
                return redirect('admin/dashboard');
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of user's blogs.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * check user type and return all blogs for admin or user's own blogs
         */
        // action button to control visibility of buttons based on role
        if ($this->user->user_type) {
            // check if author query param is set
            $author = $request->input('author');
            $blogs = $author ? Blog::latest()->where('author', $author)->get() : Blog::latest()->get();
        } else {
            $blogs = $this->user->blogs()->orderBy('updated_at', 'DESC');
        }
        $user = $this->user; // to pass to closure below
        return Datatables::of($blogs)
            ->editColumn('title', function ($data) {
                return Str::limit($data->title, 50);
            })
            ->editColumn('author', function ($data) {
                return User::find($data->author)->name;
            })
            ->editColumn('is_approved', function ($data) {
                return $data->is_approved ? 'Approved' : 'Un-approved';
            })
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                if ($user->user_type) {
                    $actionBtn = '<a href="' . route('blogs.show', $row->id) . '" class="bg-green-500 px-4 py-2 text-xs font-semibold tracking-wider text-white rounded hover:bg-green-600">View</a> ';
                } else {
                    $actionBtn = '<a href="' . route('blogs.show', $row->id) . '" class="bg-green-500 px-4 py-2 text-xs font-semibold tracking-wider text-white rounded hover:bg-green-600">View</a> <a href="' . route('blogs.edit', $row->id) . '" class="bg-blue-500 px-4 py-2 text-xs font-semibold tracking-wider text-white rounded hover:bg-blue-600">Edit</a> ';
                }
                $actionBtn .= '<button data-blogId="'.$row->id.'" class="deleteBlog bg-red-500 px-4 py-2 text-xs font-semibold tracking-wider text-white rounded hover:bg-red-600">Delete</button>';
                $actionBtn = $actionBtn;
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Blog $blog)
    {
        // running validations on the incoming data
        $validated = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);
        try {
            // throw new ErrorException('failed'); // test exception
            $newBlog = Blog::updateOrCreate(
                ['id' => $blog->id],
                [
                    'title' => $request->title,
                    'content' => $request->content,
                    'author' => $this->user->id
                ]
            );
            return back()->with('success', 'Success!');;
        } catch (\Throwable $th) {
            return back()->with(['error' => $th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('blogs.blog', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        // check if the current user is the author of the $blog
        if ($blog->author !== $this->user->id) {
            return redirect('dashboard');
        }
        return view('blogs.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        // using the store method of the class to handle both update and create events
        $this->store($request, $blog);
        // or if there's no error send the user back to same edit screen with success message
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        if ($blog->author == $this->user->id || $this->user->user_type) {
           try {
               $deleteBlog = Blog::where('id', $blog->id)->delete();
               return response()->json([
                   'status' => true,
               ]);
           } catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                ]);
           }
        } else {
            return response()->json([
                'status' => false,
            ]);
        }
    }

    /**
     * load all blogs
     *
     * @return void
     */
    public function allBlogs()
    {
        $blogs = Blog::latest()->where('is_approved', 1)->get();
        return view('blogs.all', compact('blogs'));
    }

    /**
     * return the status of the blog - approved or un-approved
     *
     * @return void
     */
    public function blogStatus($id)
    {
        $blog = Blog::findOrFail($id);
        return response()->json([
            'status' => $blog->is_approved
        ]);
    }

    /**
     * update the status of the blog - approved or un-approved
     *
     * @return void
     */
    public function updateStatus($id)
    {
        $blog = Blog::findOrFail($id);
        $blog->is_approved = (Integer)!$blog->is_approved;
        $blog->save();
        return response()->json([
            'status' => $blog->is_approved
        ]);
    }
}
