<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function home()
    {
        if (Auth::user()->user_type) {
            return redirect('/admin/dashboard');
        } else {
            $user = Auth::user()->blogs;
            return view('user.dashboard', compact('user'));
        }
    }
}
