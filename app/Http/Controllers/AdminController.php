<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function home()
    {
        $users = User::where('user_type', '0')->count();
        $blogs = count(Blog::all());
        return view('admin.dashboard', compact(['users', 'blogs']));
    }
    public function users()
    {
        return view('admin.users');
    }
    public function usersFetch()
    {
        /**
         * get all users and show them in tables
         */
        $data = User::latest()->where('user_type', '0')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $actionBtn = '<a href="'.route('admin.blogs').'?author='.$row->id.'" class="bg-green-500 px-4 py-2 text-xs font-semibold tracking-wider text-white rounded hover:bg-green-600">Blogs</a>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
