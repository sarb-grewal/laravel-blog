<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Sarb Grewal',
            'user_type' => '1',
            'email' => 'admin@sg.com',
            'password' => Hash::make('test@123'),
            'updated_at' => new DateTime(),
            'created_at' => new DateTime(),
        ]);
    }
}
