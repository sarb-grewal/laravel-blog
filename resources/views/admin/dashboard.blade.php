<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Home') }}
        </h2>
        <x-slot name="nav"></x-slot>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="rounded-t-xl overflow-hidden bg-gradient-to-r bg-white p-8">
                    <div class="grid grid-cols-2 gap-4">
                        <div class="p-5 bg-white rounded shadow-sm">
                            <div class="text-base text-gray-400 ">Users</div>
                            <div class="flex items-center pt-1">
                                <div class="text-2xl font-bold text-gray-900 ">{{$users}}</div>
                                <span class="flex items-center px-2 py-0.5 mx-2 text-sm text-green-600 bg-green-100 rounded-full">
                                    <a href="{{route('admin.users')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="green" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8l4 4m0 0l-4 4m4-4H3" />
                                        </svg>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="p-5 bg-white rounded shadow-sm">
                            <div class="text-base text-gray-400 ">Blogs</div>
                            <div class="flex items-center pt-1">
                                <div class="text-2xl font-bold text-gray-900 ">{{$blogs}}</div>
                                <span class="flex items-center px-2 py-0.5 mx-2 text-sm text-blue-600 bg-blue-100 rounded-full">
                                    <a href="{{route('admin.blogs')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="blue" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8l4 4m0 0l-4 4m4-4H3" />
                                        </svg>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>