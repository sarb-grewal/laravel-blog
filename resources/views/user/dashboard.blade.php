<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @if (!$errors)
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                        <strong class="font-bold">{{$error}}</strong>
                    </div>
                </div>
                @endif
                <div class="grid grid-cols-2 gap-4">
                    <div class="p-5 bg-white rounded shadow-sm">
                        <div class="text-base text-gray-400 ">My Blogs</div>
                        <div class="flex items-center pt-1">
                            <div class="text-2xl font-bold text-gray-900 ">{{count($blogs)}}</div>
                            <span class="flex items-center px-2 py-0.5 mx-2 text-sm text-blue-600 bg-blue-100 rounded-full">
                                <a href="{{route('my.blogs')}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="blue" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8l4 4m0 0l-4 4m4-4H3" />
                                    </svg>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>