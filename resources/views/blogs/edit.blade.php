<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ request()->routeIs('blogs.create') ? 'Add new blog' : 'Edit blog' }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if ($errors->any())
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                <strong class="font-bold">Watch Out!</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('error'))
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                {!!Session::get('error')!!}
            </div>
            @endif
            @if(Session::has('success'))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
                {!!Session::get('success')!!}
            </div>
            @endif
            <form method="POST" action="{{empty($blog) ? route('blogs.store') : route('blogs.update', $blog->id)}}">
                @if (!empty($blog))
                @method('PUT')
                @endif
                {{ csrf_field() }}
                <div class="form-group text-right">
                    <button class="py-2 px-4 my-5 bg-green-500 text-white font-semibold rounded-lg shadow-md focus:outline-none">Save</button>
                </div>
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-5">
                    <div class="form-group mb-10">
                        <input required placeholder="Blog title" type="text" name="title" class="rounded-sm w-full" value="{{ !empty($blog) ? old('title', $blog->title) : ''}}">
                    </div>
                    <div class="form-group">
                        <input id="x" type="hidden" name="content">
                        <trix-editor input="x">
                            {!! (!empty($blog) ? old('content', $blog->content) : '') !!}
                        </trix-editor>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @push('scripts')
    <script src="{{asset('plugins/trix-main/dist/trix.js')}}"></script>
    @endpush
    @push('styles')
    <link rel="stylesheet" href="{{asset('plugins/trix-main/dist/trix.css')}}" />
    @endpush
</x-app-layout>