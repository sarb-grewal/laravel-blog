<x-app-layout>
    @push('styles')
    <style>
        button[disabled] {
            cursor: not-allowed;
            pointer-events: all !important;
            opacity: 0.5;
        }
    </style>
    @endpush
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ Auth::user() && !Auth::user()->user_type ? 'Manage your blogs' : 'Manage blogs'}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (Auth::user() && !Auth::user()->user_type)
            <div class="text-right">
                <a href="{{route('blogs.create')}}" class="inline-block mb-5 py-2 px-4 bg-green-500 text-white font-semibold rounded-lg shadow-md focus:outline-none">Add
                    Blog</a>
            </div>
            @endif
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="table table-bordered yajra-datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Approved?</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var author = "{{ !empty(Request::get('author')) ? '?author='.Request::get('author') : ''  }}";
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/blogs" + author,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'author',
                        name: 'author'
                    },
                    {
                        data: 'is_approved',
                        name: 'is_approved'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
        $(document).on('click', '.deleteBlog', function(e) {
            let verify = confirm('Are you sure you want to delete this blog?');
            if (verify) {
                $(this).attr('disabled', 'disabled')
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        id: $(this).data('blogid'),
                        _method: 'DELETE',
                        _token: "{{csrf_token()}}"
                    },
                    url: "{!! env('APP_URL').'/blogs/' !!}" + $(this).data('blogid'),
                }).done(function(data) {
                    if (data.status == true) {
                        alert('Blog post deleted!')
                    } else {
                        alert('Not authorized to delete this post!')
                    }
                    $('.deleteBlog[disabled]').removeAttr('disabled');
                    window.location.reload();
                })
            }
        })
    </script>
</x-app-layout>