<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Blog Summary') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex" id="app">
                        <h1 class="text-2xl mb-5 mr-5">{{$blog->title}}</h1>
                        @if( Auth::user() && Auth::user()->user_type)
                        <Approve id="{{$blog->id}}" />
                        @else
                        @if (Auth::user() && $blog->author == Auth::user()->id)
                        <span class="text-green-500 px-4 py-2 text-xs font-semibold tracking-wider rounded">
                            <a href="{{route('blogs.edit', $blog->id)}}">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                </svg>
                            </a>
                        </span>
                        @endif
                        @endif
                    </div>
                    <p class="italic">{{$blog->user->name}}</p>
                    <p>Last Updated at : {{date('d-m-Y H:i', strtotime($blog->updated_at))}}</p>
                    <form action="">

                    </form>
                    <hr class="my-2">
                    <div class="content">
                        {!!$blog->content!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>