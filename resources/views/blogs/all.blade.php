<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All Blogs') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(!empty($blogs))
                @foreach ($blogs as $item)
                    <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-5 mb-5">
                        <h2 class="text-2xl mb-5">{{Str::limit($item->title, 50)}}</h2>
                        <a class="inline-block p-2 rounded-md shadow-sm hover:bg-blue-500 hover:text-white bg-blue-100"
                            href="{{route('blogs.show', $item->id)}}">Read More</a>
                    </div>
                @endforeach
            @else
            <h2 class="text-2xl mb-5">No blogs yet!</h2>
            @endif
        </div>
    </div>
</x-app-layout>