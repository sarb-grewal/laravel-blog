require('./bootstrap');

require('alpinejs');

import Vue from 'vue'
import Approve from './components/approve';

(function () {
    addEventListener("trix-file-accept", function (event) {
        event.preventDefault();
        alert('File uploading would have taken too long! So I skipped it');
    })
})();

const vue = new Vue({
    el: '#app',
    components: {Approve}
})