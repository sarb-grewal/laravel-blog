<?php

use App\Models\Blog;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    $blogs = Blog::latest()->where('is_approved', 1)->get();
    return view('blogs.all', compact('blogs'));
});

Route::group(['middleware' => 'isAdmin', 'prefix' => 'admin'], function () {
    Route::get('/dashboard', 'App\Http\Controllers\AdminController@home')->name('admin.dashboard');
    Route::get('/users', 'App\Http\Controllers\AdminController@users')->name('admin.users');
    Route::get('/users/lists', 'App\Http\Controllers\AdminController@usersFetch')->name('admin.users.list');
    Route::get('/blogs', function () {
        return view('blogs.blogs');
    })->name('admin.blogs');
});

Route::get('/blogs/all', 'App\Http\Controllers\BlogController@allBlogs')->name('all.blogs');
Route::get('/blogs/{id}/status', 'App\Http\Controllers\BlogController@blogStatus')->middleware('auth');
Route::put('/blogs/{id}/status', 'App\Http\Controllers\BlogController@updateStatus')->middleware('auth');
Route::resources([
    'blogs' => \App\Http\Controllers\BlogController::class,
]);
Route::get('/dashboard', function () {
    return view('blogs.blogs');
})->name('dashboard')->middleware('auth');
require __DIR__ . '/auth.php';
