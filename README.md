Simple Blog site in Laravel, which features

1. User authentication
2. Blog approval by the admin

# How to setup

1. Install composer dependencies by running the following command:
    `composer install`

2. Install node modules by running:
    `npm i`

3. Compile js and css assets
    `npm run dev`

### Run migrations and seed initial data

4. Run the following commands sequentially
    `php artisan migrate`
    `php artisan db:seed`

5. Update appropriate `APP_URL` in `.env` file

6. Serve the application with
    `php artisan serve`
    
### Toggle status of post between 'approved' and 'un-approved' (only available in Admin dashboard)
![Image](https://prnt.sc/12bvuxr)